<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Author_ID {

	public $return_data = '';
    
	public function Author_ID()
	{
		$this->EE =& get_instance();
		
	}
	
	// ----------------------------------------------------------------


	/** 
	 * Converts screen name to URL friendly
	*/	
	
	function get_screen_name() {
		$screen_name = $this->EE->TMPL->tagdata;
		$separator = $this->EE->TMPL->fetch_param('separator') ?: '-'; 
		$this->return_data = strtolower($screen_name);	
		$this->return_data = preg_replace('/[^\w\-'.']+/u', $separator, $this->return_data);
		return $this->return_data;
	}

	/** 
	* Returns author ID from URL segment 
	*/
	
	function get_id() {
		$separator = $this->EE->TMPL->fetch_param('separator') ?: '-'; 
		$screen_name = $this->EE->TMPL->fetch_param('screen_name') ?: '';
		// If no group is specified, set to 0
		$group_id = $this->EE->TMPL->fetch_param('group_id') ?: '0';
		// Remove URL separator
		$screen_name = str_replace($separator, ' ', $screen_name);
		if ($group_id>0) { 
			$member = ee('Model')->get('Member')
			->filter('screen_name', $screen_name)
			->filter('group_id', $group_id)
			->first();
			}
		else { 
			$member = ee('Model')->get('Member')
			->filter('screen_name', $screen_name)
			->first();
			}
		//ee()->load->library('logger');
		//ee()->logger->developer('Author ID ' . var_dump($member->member_id));	
		$this->return_data = $member->member_id ?: '';
		return $this->return_data;	
	}
	
}